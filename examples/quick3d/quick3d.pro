TEMPLATE = subdirs
SUBDIRS += \
    customshaders \
    effects \
    hellocube \
    simple \
    view3d \
    picking \
    blendmodes \
    lights \
    custommaterial \
    principledmaterial \
    dynamictexture \
    dynamiccreation
