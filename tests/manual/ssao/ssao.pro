QT += quick quick3d-private

SOURCES += \
    ssao.cpp

RESOURCES += \
    ssao.qrc

OTHER_FILES += \
    ssao.qml
